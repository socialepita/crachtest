package fr.dotty.app.myapptest;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.SeekBar;

public class WhyNotActivity extends Activity {

    public SeekBar seekBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.why_not_activity);
        seekBar = findViewById(R.id.seekBar);
        Intent intent = getIntent();
        seekBar.setProgress(intent.getExtras().getInt("pgrs", 0));
    }

    public void Back(View view)
    {
        Intent data = new Intent();
        data.putExtra("pgrs",seekBar.getProgress());
        setResult(RESULT_OK, data);
        finish();
    }
}
