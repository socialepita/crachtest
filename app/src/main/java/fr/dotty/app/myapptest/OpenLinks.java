package fr.dotty.app.myapptest;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.TextView;

public class OpenLinks extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_open_links);
        Intent intent = getIntent();
        Uri data = intent.getData();
        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setText(data.toString());
    }
}
