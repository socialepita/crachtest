package fr.dotty.app.myapptest;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    public int lastPgrs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Context context = this.getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences("settings.txt", Context.MODE_PRIVATE);
        lastPgrs = sharedPref.getInt("pgrs", 0);
    }

    /** Called when the user taps the Send button */
    public void ChangeActivity(View view) {
        Intent intent = new Intent(this, WhyNotActivity.class);
        intent.putExtra("pgrs",lastPgrs);
        startActivityForResult(intent, 42);
    }

    public void cute(View view)
    {
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://goo.gl/VhWX4c"));
        startActivity(intent);
    }

    public void savePgrs(View view)
    {
        Context context = this.getApplicationContext();
        SharedPreferences sharedPref = context.getSharedPreferences("settings.txt", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt("pgrs", lastPgrs);
        editor.commit();
    }

    public void gotoDrawerLayout(View view)
    {
        Intent intent = new Intent(this, MyDrawerLayout.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        Button buttonMain = (Button) findViewById(R.id.buttonMain);

        if (requestCode == 42) {
            if (resultCode == RESULT_OK) {
                if(data.hasExtra("pgrs"))
                {
                    int progress = Integer.parseInt(data.getExtras().get("pgrs").toString());
                    this.lastPgrs = progress;
                    buttonMain.setText("Progress was: "+progress+"%");
                }
            }
        }
    }

}
