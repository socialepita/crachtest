package fr.dotty.app.myapptest;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;

import java.util.Locale;

public class MyDrawerLayout extends Activity {

    private String[] leftDrawerListNames;
    private DrawerLayout leftDrawerLayout;
    private ListView leftDrawerListView;

    //region private class

    /**
     * Fragment that appears in the "content_frame", shows a planet
     */
    public static class PlanetFragment extends Fragment {
        public static final String ARG_PLANET_NUMBER = "planet_number";

        public PlanetFragment() {
            // Empty constructor required for fragment subclasses
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View rootView = inflater.inflate(R.layout.fragment_planet, container, false);
            int i = getArguments().getInt(ARG_PLANET_NUMBER);
            String planet = getResources().getStringArray(R.array.left_menu)[i];

            int imageId = getResources().getIdentifier(planet.toLowerCase(Locale.getDefault()),
                    "drawable", getActivity().getPackageName());
            ((ImageView) rootView.findViewById(R.id.image)).setImageResource(imageId);
            getActivity().setTitle(planet);
            return rootView;
        }
    }

    private enum LEFTDRAWER{
        CHRONOS(0), HOMEWORK(1), MESSAGES(2), SETTINGS(3);
        public final int code;
        LEFTDRAWER(int code){this.code = code;}
        public int toInt(){return code;}
        public String toString() {return String.valueOf(code);}
    }

    private void selectItem(int position) {
        // Create a new fragment and specify the planet to show based on position
        //Fragment fragment = new PlanetFragment();
        //Bundle args = new Bundle();
        //args.putInt(PlanetFragment.ARG_PLANET_NUMBER, position);
        //fragment.setArguments(args);

        // Insert the fragment by replacing any existing fragment
        //FragmentManager fragmentManager = getFragmentManager();
        //fragmentManager.beginTransaction()
        //        .replace(R.id.content_frame, fragment)
        //        .commit();

        switch(LEFTDRAWER.values()[position])
        {
            case CHRONOS:
                chronos();
                break;
            default:
        }

        // Highlight the selected item, update the title, and close the drawer
        leftDrawerListView.setItemChecked(position, true);
        setTitle(leftDrawerListNames[position]);
        leftDrawerLayout.closeDrawer(leftDrawerListView);
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    //endregion

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_drawer_layout);

        leftDrawerListNames = getResources().getStringArray(R.array.left_menu);
        leftDrawerLayout = findViewById(R.id.left_drawer_layout);
        leftDrawerListView = findViewById(R.id.left_drawer);

        leftDrawerListView.setAdapter(new ArrayAdapter<String>(this,
                R.layout.drawer_list_item, leftDrawerListNames));
        leftDrawerListView.setOnItemClickListener(new DrawerItemClickListener());
    }

    public void chronos()
    {
        Intent chronos = new Intent(Intent.ACTION_VIEW, Uri.parse("http://chronos.epita.net/"));
        startActivity(chronos);
    }

}
